# [D2 Advance](https://github.com/d2-projects/d2-advance)

[![last release](https://img.shields.io/github/v/release/d2-projects/d2-advance?style=flat-square)](https://github.com/d2-projects/d2-advance/releases)
[![Codacy grade](https://img.shields.io/codacy/grade/6d94e3a406574c3cbef6e7157ccd21a2?label=quality&logo=codacy&style=flat-square)](https://app.codacy.com/gh/d2-projects/d2-advance/dashboard)
[![ci](https://img.shields.io/github/workflow/status/d2-projects/d2-advance/Release%20pipeline?logo=github&style=flat-square)](https://github.com/d2-projects/d2-advance/actions)
[![last commit](https://img.shields.io/github/last-commit/d2-projects/d2-advance?logo=git&logoColor=white&style=flat-square)](https://github.com/d2-projects/d2-advance/commits/master)

> 🧗Advanced, colourful front-end integration practice. be inspired by D2Admin :)

Preview here 👉 [https://d2.pub/d2-advance/preview](https://d2.pub/d2-advance/preview)

## Documents

[简体中文](docs/zh/README.md)

## Public Repositories

*   [Github](https://gitee.com/d2-projects/d2-advance)
*   [码云](https://gitee.com/d2-projects/d2-advance) (mirror)

## Related

*   [d2-projects/d2-admin](https://github.com/d2-projects/d2-admin)
*   [MZCretin/RollToolsApi](https://github.com/MZCretin/RollToolsApi)

## License

D2Advance is licensed under a [MIT License](./LICENSE).
